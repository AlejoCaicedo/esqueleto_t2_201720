package model.logic;

public class Stops{
	
	private String stop_id,stop_code,stop_name,stop_desc,stop_lat;
	private String stop_lon,zone_id,stop_url,location_type,parent_station;
	
	public Stops(String stop_id, String stop_code, String stop_name, String stop_desc, String stop_lat,
			String stop_lon, String zone_id, String stop_url, String location_type, String parent_station)
	{
		this.stop_id = stop_id;
		this.stop_code = stop_code;
		this.stop_name = stop_name;
		this.stop_desc = stop_desc;
		this.stop_lat = stop_lat;
		this.stop_lon = stop_lon;
		this.zone_id = zone_id;
		this.stop_url = stop_url;
		this.location_type = location_type;
		this.parent_station = parent_station; 
	}
	
	public String getStopId()
	{
		return stop_id;
	}
	
	public String getStopName()
	{
		return stop_name;
	}
}
