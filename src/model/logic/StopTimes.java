package model.logic;

public class StopTimes {

	private String trip_id,arrival_time,departure_time,stop_id;
	private String stop_sequence,stop_headsign,pickup_type,drop_off_type,shape_dist_traveled;

	public StopTimes(String trip_id, String arrival_time, String departure_time, String stop_id,
			String stop_sequence, String stop_headsign, String pickup_type, String drop_off_type, String shape_dist_traveled)
	{
		this.trip_id = trip_id;
		this.arrival_time = arrival_time;
		this.departure_time = departure_time;
		this.stop_id = stop_id;
		this.stop_sequence = stop_sequence;
		this.stop_headsign = stop_headsign;
		this.pickup_type = pickup_type;
		this.drop_off_type = drop_off_type;
		this.shape_dist_traveled = shape_dist_traveled;
	}
	
	
	public String getStopId()
	{
		return stop_id;
	}
	
	public String getTripId()
	{
		return trip_id;
	}
}
