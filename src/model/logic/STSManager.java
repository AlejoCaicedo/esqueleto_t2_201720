package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.data_structures.IList;

public class STSManager implements ISTSManager {

	private List<Routes> Routes;
	private List<Trip> Trips;
	private List<StopTimes> StopTimesList;
	private List<Stops> StopsList;

	@Override
	public void loadRoutes(String routesFile) throws Exception {

		try{
		BufferedReader in = new BufferedReader(new FileReader(routesFile));
		Routes = new LinkedList<Routes>();
		String tempLine;
					tempLine = in.readLine();//Este es la linea que dice el formato
			while((tempLine = in.readLine()) != null)
			{
				String[] data = tempLine.split(",");
				if(data.length == 9)
				{
					
					Routes dataPoint = new Routes(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8]);
					Routes.add(dataPoint);
					
				}else{
					throw new Exception("File Routes: Incorrect data syntax");
				}
			}
			in.close();
		}catch(IOException e)
		{
			throw new Exception("(File Routes: Wrong file name) " + e.getMessage());
		}
	}

	@Override
	public void loadTrips(String tripsFile) throws Exception {
		try{		
		BufferedReader in = new BufferedReader(new FileReader(tripsFile));
		Trips = new LinkedList<Trip>();
		String tempLine;
				tempLine = in.readLine();//Este es la linea que dice el formato
			while((tempLine = in.readLine()) != null)
			{
				String[] data = tempLine.split(",");
				if(data.length == 10)
				{
					
					Trip dataPoint = new Trip(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8], data[10]);
					Trips.add(dataPoint);
					
				}else{
					throw new Exception("File Trips: Incorrect data syntax");
				}
			}
			in.close();
		}catch(IOException e)
		{
			throw new Exception("(File Trips: Wrong file name) " + e.getMessage());
		}
	}

	@Override
	public void loadStopTimes(String stopTimesFile) throws Exception{
		try{		
			BufferedReader in = new BufferedReader(new FileReader(stopTimesFile));
			StopTimesList = new LinkedList<StopTimes>();
			String tempLine;
						tempLine = in.readLine();//Este es la linea que dice el formato
				while((tempLine = in.readLine()) != null)
				{
					String[] data = tempLine.split(",");
					if(data.length == 9)
					{
						
						StopTimes dataPoint = new StopTimes(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8]);
						StopTimesList.add(dataPoint);
						
					}else{
						throw new Exception("File StopTimes: Incorrect data syntax");
					}
				}
				in.close();
			}catch(IOException e)
			{
				throw new Exception("(File StopTimes: Wrong file name) " + e.getMessage());
			}
	}

	@Override
	public void loadStops(String stopsFile) throws Exception {
		try{		
			BufferedReader in = new BufferedReader(new FileReader(stopsFile));
			StopsList = new LinkedList<Stops>();
			String tempLine;
						tempLine = in.readLine();//Este es la linea que dice el formato
				while((tempLine = in.readLine()) != null)
				{
					String[] data = tempLine.split(",");
					if(data.length == 10)
					{
						
						Stops dataPoint = new Stops(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8], data[9]);
						StopsList.add(dataPoint);
						
					}else{
						throw new Exception("File Stops: Incorrect data syntax");
					}
				}
				in.close();
			}catch(IOException e)
			{
				throw new Exception("(File Stops: Wrong file name) " + e.getMessage());
			}
	}

	@Override
	public List<VORoute> routeAtStop(String stopName) throws Exception {
		/*
		 * Los pasos que voy a usar son:
		 * 1. En el archivo stops buscar el Stop id usando el StopName
		 * 2. En stopTimes buscar cuales trips pasan por ese stop
		 * 3. En Trips busco que rutas que pasasn por el trip
		 * 4. Con el routeId busco cual es su nombre, genero un VORoute y lo agrego a la lista de retorno
		 * 5. Retorno la lista con todos los VORoute 
		 */
		// PASO 1:
		LinkedList<VORoute> retorno = new LinkedList<VORoute>();
		
		for(Stops tempStop : StopsList)
		{
			if((tempStop.getStopName()).equals(stopName))
			{
				String stopId = tempStop.getStopId();
				//nunca convierto los Strings a integers hasta el puro final porque es m�s trabajo para la maquina
				for(StopTimes tempStopTimes : StopTimesList)
				{
					// PASO 2:
					if((tempStopTimes.getStopId()).equals(stopId))
					{
						String tripId = tempStopTimes.getStopId();
						//Paso 3:
						for(Trip tempTrip : Trips)
						{
							if(tripId.equals(tempTrip.getTripId()))
							{
								// PASO 4:
								String routeId = tempTrip.getRouteId();
								for(Routes tempRoute : Routes)
								{
									if(routeId.equals(tempRoute.getRouteId()))
									{
										//Paso 5:
										VORoute tempReturn = new VORoute(Integer.parseInt(routeId), tempRoute.getRouteName());
										retorno.add(tempReturn);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}

	@Override
	public List<VOStop> stopsRoute(String routeName, String direction) {
		
		/*
		 *PASO 1: Busco en routes el routeId
		 *PASO 2: Con el routeId en trips busco los trips que cuadren con la ruta y direccion que nos dieron
		 *		  donde consigo una lista de trips
		 *PASO 3: En stopTimes busco los stops que cuadran con los trips
		 *p.s: no organizo los stops que saco de stopTimes, porque ya estan organizados en orden de salida :)
		 *PASE 4: Retorno una lista de Stops
		 */
		LinkedList<VOStop> retorno = new LinkedList<VOStop>();
		//PASO 1[.
				for(Routes tempRoutes : Routes)
				{
					String routeId = tempRoutes.getRouteId();
						//PASO 2[.
						for(Trip tempTrip : Trips)
						{
							if(routeId.equals(tempTrip.getRouteId()) && (tempTrip.getDir()).equals(direction))
							{
								String tripId = tempTrip.getTripId();
								//PASo 3[.
								for(StopTimes tempStopList : StopTimesList)
								{
									if((tempStopList.getTripId()).equals(tripId))
									{
										for(Stops tempStop : StopsList)
										{
											if((tempStopList.getStopId()).equals(tempStop.getStopId()))
											{
												VOStop agregar = new VOStop(tempStop.getStopId(),tempStop.getStopName());
												retorno.add(agregar);
												break;
											}
										}
									}
								}
							}
						}
				}
		
		
		return retorno;
	}

	
	
	
	
	
	
	
	
}
