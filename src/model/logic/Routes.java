package model.logic;


public class Routes {
	/*
	 * route_id,agency_id,route_short_name,
	 * route_long_name,route_desc,route_type,route_url,route_color,route_text_color
	 */
	private String route_id, agency_id,route_short_name,route_long_name;
	private String route_desc,route_url,route_color,route_text_color;
	private String route_type;
	
	public Routes(String route_id, String agency_id, String route_short_name, String route_long_name,
			String route_type, String route_desc, String route_url, String route_color, String route_text_color)
	{
		this.route_id = route_id;
		this.agency_id = agency_id;
		this.route_short_name = route_short_name;
		this.route_long_name = route_long_name;
		this.route_type = route_type;
		this.route_desc = route_desc;
		this.route_url = route_url;
		this.route_color = route_color;
		this.route_text_color = route_text_color;
	}

	@Override//El toString fue generado por eclipse
	public String toString() {
		return "Routes [route_id=" + route_id + ", agency_id=" + agency_id + ", route_short_name=" + route_short_name
				+ ", route_long_name=" + route_long_name + ", route_desc=" + route_desc + ", route_url=" + route_url
				+ ", route_color=" + route_color + ", route_text_color=" + route_text_color + ", route_type="
				+ route_type + "]";
	}
	
	public String getRouteId()
	{
		return route_id;
	}
	
	public String getRouteName()
	{
		return route_long_name;
	}
	
	
}
