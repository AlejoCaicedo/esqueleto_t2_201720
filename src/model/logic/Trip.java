package model.logic;

public class Trip {

	private String route_id,service_id,trip_id,trip_headsign;
	private String trip_short_name,direction_id,block_id,shape_id,wheelchair_accessible,bikes_allowed; 

	public Trip(String route_id, String service_id, String trip_id, String trip_headsign,
			 String trip_short_name, String direction_id, String block_id, String shape_id, String wheelchair_accessible, String bikes_allowed)
	{
		 this.route_id = route_id;
		 this.service_id = service_id;
		 this.trip_id = trip_id;
		 this.trip_headsign = trip_headsign;
		 this.trip_short_name = trip_short_name;
		 this.direction_id = direction_id;
		 this.block_id = block_id;
		 this.shape_id = shape_id;
		 this.wheelchair_accessible = wheelchair_accessible;
		 this.bikes_allowed = bikes_allowed;
	}
	
	public String getTripId()
	{
		return trip_id;
	}
	
	public String getRouteId()
	{
		return route_id;
	}
	public String getDir()
	{
		return direction_id;
	}
}
