package model.vo;

/**
 * Representation of a route object
 */
public class VORoute {

	private int Id;
	private String Name;
	

	public VORoute(int id, String name)
	{
		this.Id = id;
		this.Name = name;
	}
	
	/**
	 * @return id - Route's id number
	 */
	public int id() {
		// TODO Auto-generated method stub
		return Id;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return Name;
	}
	
	public void setId(int id)
	{
		this.Id = id;
	}
	
	public void setName(String name)
	{
		this.Name = name;
	}

}
