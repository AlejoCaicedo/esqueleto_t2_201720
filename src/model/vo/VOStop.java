package model.vo;


/**
 * Representation of a Stop object
 */
public class VOStop {

	private int id;
	private String name;
	
	public VOStop(String id, String name)
	{
		this.id = Integer.parseInt(id);
		this.name = name;
	}
	
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		return id;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		return name;
	}

}
