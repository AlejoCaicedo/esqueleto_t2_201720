package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T> implements IList<T>
{	
    private Node<T> primero;
    
	private Node<T> ultimo;
	
	private Iterator<T> iterador;
	
	private Integer tamano;
	
	public DoubleLinkedList ( )
	{
		tamano = 0;
		primero = null;
		ultimo = primero;
	}

	public Iterator<T> iterator( )
	{
		return new Iterador<T>(primero);
	}	
	
	public Node<T> darPrimero()
	{
		return primero;
	}
	
	public Node<T> darUltimo()
	{
		return ultimo;
	}
	
	public Iterator<T> darIterador( )
	{
		return iterador;
	}
	 
	public Integer getSize( ) 
	{
		return tamano;
	}
	 
	public void add(T objeto)
	{
		Node<T> extra = new Node<T>( ); 
		extra.cambiarObjeto(objeto);
		if(primero != null)
		{			
			extra.cambiarSiguiente(primero);
			primero.cambiarAnterior(extra);
			primero = extra;
			iterador = iterator( );
		}
		else
		{
			primero = extra;
			ultimo = extra;
			iterador = iterator( );
		}	
		tamano++;
	}
	 
	public void addAtEnd(T objeto)
	{
		Node<T> extra = new Node<T>(); 
		extra.cambiarObjeto(objeto);
		if(ultimo!=null)
		{			
			extra.cambiarAnterior( ultimo );
			ultimo.cambiarSiguiente( extra );
			ultimo = extra;
		}
		else
		{
			ultimo = extra;
			primero = extra;
			iterador = iterator( );
		}	
		tamano++;
	}
	 
	public void addAtK(T objeto, Integer posicion)
	{
		if(posicion==0)		
			add(objeto);		
		else if(posicion==(tamano-1))
			addAtEnd(objeto);
		else
		{
			Integer contador = 0;
			Node<T> extra = new Node<T>(); 
			extra.cambiarObjeto(objeto);
			while( iterador.hasNext() && posicion-1 > contador)
			{
				contador++;
				iterador.next();
			}	
			Node<T> actual=((Iterador<T>) iterador).darActual();
			extra.cambiarSiguiente(actual.darSiguiente());
			extra.darSiguiente( ).cambiarAnterior(extra);
			extra.cambiarAnterior(actual);
			extra.darAnterior( ).cambiarSiguiente(extra);
		}
		iterador = iterator( );
		tamano++;
	}
	 
	public T getElement(Integer posicion) 
	{
		Integer contador = 0;
		while( iterador.hasNext( ) && posicion > contador )
		{
			contador++;
			iterador.next( );
		}		
		T element = iterador.next( );
		iterador = iterator( );
		return element;
	}
	 
	public T getCurrentElement() 
	{
		return ((Iterador<T>) iterador).darActual( ).darObjeto( );
	}
	 
	public void delete(T objeto) 
	{
		if( ultimo.darObjeto( ) == objeto )
		{
			ultimo.darAnterior().cambiarSiguiente(null);
			ultimo = ultimo.darAnterior();
		}
		else if( primero.darObjeto( ) == objeto )
		{
			primero.darSiguiente().cambiarAnterior(null);
			primero=primero.darSiguiente();
		}
		else
		{
			Boolean eliminado = false;
			while( !eliminado && iterador.hasNext())
			{
				Node<T> actual = ((Iterador<T>) iterador).darActual( );
				if( actual.darSiguiente( ).darObjeto( ) == objeto )
				{
					actual.cambiarSiguiente(actual.darSiguiente( ).darSiguiente( ));
					actual.darSiguiente( ).cambiarAnterior( actual );
					eliminado = true;
				}
				iterador.next();
			}
		}
		iterador = iterator();
		tamano--;
	}
	 
	public void deleteAtK(Integer posicion) 
	{
		if(posicion == tamano-1)
		{
			ultimo.darAnterior( ).cambiarSiguiente( null );
			ultimo = ultimo.darAnterior( );
		}
		else if(posicion == 0)
		{
			primero.darSiguiente( ).cambiarAnterior( null );
			primero=primero.darSiguiente( );
		}
		else
		{
			Integer contador = 0;
			while( iterator( ).hasNext( ) && posicion-1 > contador)
			{
				contador++;
				iterador.next( );
			}		
			Node<T> actual = ((Iterador<T>) iterador).darActual( );
			actual.cambiarSiguiente(actual.darSiguiente( ).darSiguiente( ));
			actual.darSiguiente( ).cambiarAnterior( actual );
		}
		iterador = iterator ();
		tamano--;
	}
}
