package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();
	
	public void add( T param );
	
	public void addAtEnd( T param );

	public void addAtK( T param, Integer pos );
	
	public T getCurrentElement( );
	
	public T getElement( Integer pos );
	
	public void delete( T param );
	
	public void deleteAtK( Integer pos );
}
