package model.data_structures;

import java.util.Iterator;

public class RingList<T> implements IList<T> {

	private Node<T> primero;

	private Node<T> ultimo;

	private int tamano;

	private Iterator<T> iterador;

	public RingList( )
	{
		tamano = 0;
		primero = null;
		ultimo = primero;
	}

	public Iterator<T> iterator( ) 
	{
		return new IteradorAnidado<T>(primero);
	}

	public Node<T> darPrimero()
	{
		return primero;
	}

	public Node<T> darUltimo()
	{
		return ultimo;
	}

	public Integer getSize() 
	{
		return tamano;
	}


	public void add( T objeto ) 
	{
		Node<T> extra = new Node<T>( );
		extra.cambiarObjeto(objeto);
		if (primero!= null)
		{
			extra.cambiarAnterior(ultimo);
			extra.cambiarSiguiente(primero);
			primero.cambiarAnterior(extra);
			ultimo.cambiarSiguiente(extra);
			primero = extra;
			tamano++;
		}
		else 
		{
			primero = extra;
			ultimo = extra;
			tamano++;
		}

	}


	public void addAtEnd(T objeto) 
	{  
		Node<T> extra= new Node<T>( );
		extra.cambiarObjeto(objeto);
		if(primero!=null)
		{
			extra.cambiarSiguiente(primero);
			extra.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(extra);
			primero.cambiarAnterior(extra);
			ultimo = extra;
			tamano++;
		}
		else 
		{
			primero = extra;
			ultimo = extra;
			tamano++;
		}
	}

	public void addAtK( T objeto, Integer posicion ) 
	{
		if (posicion%tamano == 0 || posicion == 0)
		{
			add(objeto);
			tamano++;
		}
		else if(tamano<posicion)
		{
			int pos = posicion%tamano;
			int cont = 1;
			Node<T> temp = primero;
			boolean ind = false;
			while(temp!=null && !ind)
			{
				if ( cont == pos )
				{
					Node<T> extra = new Node<T>( );
					extra.cambiarObjeto(objeto);
					Node<T> temp2 = temp.darSiguiente( );
					extra.cambiarSiguiente(temp2);
					extra.cambiarAnterior(temp);
					temp.cambiarSiguiente(extra);
					temp2.cambiarAnterior(extra);
					ind = true;
				}
				temp = temp.darSiguiente( );
				cont++;
			}
			tamano++;
		}
		else
		{
			int cont=1;
			Node<T> temp = primero;
			boolean ind = false;
			while(temp!=null && !ind)
			{
				if ( posicion == cont )
				{
					Node<T> extra = new Node<T>( );
					extra.cambiarObjeto(objeto);
					Node<T> temp2 = temp.darSiguiente( );
					extra.cambiarSiguiente(temp2);
					extra.cambiarAnterior(temp);
					temp.cambiarSiguiente(extra);
					temp2.cambiarAnterior(extra);
					ind = true;
				}
				temp = temp.darSiguiente( );
				cont++;
			}
			tamano++;
		}
	}


	public T getElement(Integer posicion) 
	{
		Node<T> buscado = null;
		if ( posicion%tamano == 0 || posicion == 0)
		{
			buscado = primero;
		}
		else if(tamano<posicion)
		{
			int pos = posicion%tamano;	
			int cont = 0;
			Node<T> temp = primero;
			boolean ind = false;
			while(temp!=null && !ind)
			{
				if ( cont == pos )
				{
					buscado = temp;
					ind = true;
				}
				temp = temp.darSiguiente( );
				cont++;
			}
		}
		else
		{
			int cont = 0;
			Node<T> temp =primero;
			boolean ind = false;
			while(temp!=null && !ind)
			{
				if ( posicion == cont )
				{
					buscado=temp;
					ind = true;;
				}
				temp = temp.darSiguiente( );
				cont++;
			}
		}
		return buscado.darObjeto( );
	}

	public T getCurrentElement() 
	{
		return ((Iterador<T>) iterador).darActual( ).darObjeto( );
	}

	public void delete(T objeto) 
	{
		if (ultimo.darObjeto( ) == objeto)
		{
			Node<T> temp = ultimo.darAnterior( );
			temp.cambiarSiguiente(primero);
			primero.cambiarAnterior(temp);
			ultimo = temp;
			tamano--;
		}
		else if (primero.darObjeto( ) == objeto)
		{
			Node<T> temp = primero.darSiguiente( );
			ultimo.cambiarSiguiente(temp);
			temp.cambiarAnterior(ultimo);
			primero = temp;
			tamano--;
		}
		else
		{
			Node<T> temp = primero;
			boolean ind = false;
			while (temp!=null && !ind)
			{
				if (temp.darObjeto( ) == objeto)
				{
					Node<T> temp2 = temp.darSiguiente( );
					Node<T> temp3 = temp.darAnterior( );
					temp2.cambiarAnterior(temp3);
					temp3.cambiarSiguiente(temp2);
					ind = true;
				}
				temp = temp.darSiguiente( );
			}
			tamano--;
		}
	}
	
	public void deleteAtK(Integer posicion) 
	{  	
		if (posicion%tamano==0 || posicion==0)
		{
			Node<T> temp = primero.darSiguiente( );
			ultimo.cambiarSiguiente(temp);
			temp.cambiarAnterior(ultimo);
			primero = temp;
			tamano--;
		}
		else if(tamano<posicion)
		{
			int pos = posicion%tamano;
			int cont = 0;
			Node<T> temp = primero;
			boolean ind = false;
			while (temp!=null && !ind)
			{
				if(cont==pos)
				{
					Node<T> temp2 = temp.darSiguiente( );
					Node<T> temp3 = temp.darAnterior( );
					temp2.cambiarAnterior(temp3);
					temp3.cambiarSiguiente(temp2);
					ind = true;
				}
				temp = temp.darSiguiente( );
				cont++;
			}
			tamano--;
		}
		else
		{
			int cont = 0;
			Node<T> temp = primero;
			boolean ind = false;
			while (temp!=null && !ind)
			{
				if (cont==posicion)
				{
					Node<T> temp2 = temp.darSiguiente( );
					Node<T> temp3 = temp.darAnterior( );
					temp2.cambiarAnterior(temp3);
					temp3.cambiarSiguiente(temp2);
					ind = true;;
				}
				temp = temp.darSiguiente( );
				cont++;
			}
			tamano--;
		}
	}

}