package model.data_structures;

import java.util.Iterator;

public class Iterador <T> implements Iterator<T>
{
	private Node<T> actual;
	
	public Iterador(Node<T> iter) 
	{
		actual = iter;
	}	

	public Node<T> darActual()
	{
		return actual;
	} 
	 
	public boolean hasNext()
	{
		return (actual!=null && actual.darSiguiente( )!=null);
	}
	 
	public T next()
	{
		actual = actual.darSiguiente( );
		T objeto = actual.darObjeto( );
		return objeto;
	}
}
