package model.data_structures;

import java.util.Iterator;

public class IteradorAnidado<T> implements Iterator<T>
{
	private Node<T> actual;

	public IteradorAnidado(Node<T> param)
	{
		actual = param;
	}
	
	public boolean hasNext( )
	{
		return (actual!=null && actual.darSiguiente( )!=null) ;
	}
	
	public T next( )
	{
		actual = actual.darSiguiente( );
		T objeto = actual.darObjeto( );
		return objeto;
	}
}