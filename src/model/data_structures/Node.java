package model.data_structures;

public class Node<T>
{	
		private T Objeto ;
		
		private Node<T> siguiente;
		
		private Node<T> anterior;
		
		public Node()
		{
			Objeto = null;
			siguiente = null;
			anterior = null;
		}	
		
		public void cambiarObjeto(T pObjeto)
		{
			Objeto = pObjeto;
		}
				
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		
		public Node<T> darAnterior()
		{
			return anterior;
		}

		public T darObjeto()
		{
			return Objeto;
		}		
		
		public void cambiarSiguiente(Node<T> nuevoSig)
		{
			siguiente = nuevoSig;
		}
		
		public void cambiarAnterior(Node<T> nuevoAnt)
		{
			anterior = nuevoAnt;
		}

}
