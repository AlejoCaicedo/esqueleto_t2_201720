package controller;

import java.util.List;

import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();
	
	public static void loadRoutes() {
		try{
		manager.loadRoutes("data/routes.txt");
		}catch(Exception e)
		{
			
		}
	}
		
	public static void loadTrips() {
		try{
			manager.loadTrips("data/trips.txt");
			}catch(Exception e)
			{
				
			}
	}

	public static void loadStopTimes() {
		try{
			manager.loadStopTimes("data/stop_times.txt");
			}catch(Exception e)
			{
				
			}
	}
	
	public static void loadStops() {
		try{
			manager.loadStops("data/stops.txt");
			}catch(Exception e)
			{
				
			}
	}
	
	public static List<VORoute> routeAtStop(String stopName) {
		try{
		return manager.routeAtStop(stopName);
		}catch(Exception e)
		{
		 return null;	
		}
	}
	
	public static List<VOStop> stopsRoute(String routeName, String direction) {
		try{
			return manager.stopsRoute(routeName, direction);
			}catch(Exception e)
			{
			 return null;	
			}
	}
}
